﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using Mpsi.Mobile.Core.Services.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mpsi.Mobile.Core.Utility.MarkupExtensions
{
    // You exclude the 'Extension' suffix when using in Xaml markup
    // How to use:
    // Add:         xmlns:i18n="clr-namespace:UsingResxLocalization;assembly=UsingResxLocalization"
    // Localize:    <Button Text="{i18n:TranslateExtension Text=AddButton}" />

    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        readonly CultureInfo _ci;
        // TODO: Lea: Find better way to get resource base name
        private const string ResourceBaseName = "Mpsi.Mobile.Core.Resources.AppResources";

        public TranslateExtension()
        {
            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                _ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            }
        }

        public string Text { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return "";

            var resmgr = new ResourceManager(ResourceBaseName, typeof(TranslateExtension).GetTypeInfo().Assembly);

            var translation = resmgr.GetString(Text, _ci);

            if (translation == null)
            {
                translation = Text; // HACK: returns the key, which GETS DISPLAYED TO THE USER

//#if DEBUG
//                throw new ArgumentException(
//                    $"Key '{Text}' was not found in resources '{ResourceBaseName}' for culture '{_ci.Name}'.", "Text");
//#else
//                translation = Text; // HACK: returns the key, which GETS DISPLAYED TO THE USER
//#endif
            }
            return translation;
        }
    }
}
