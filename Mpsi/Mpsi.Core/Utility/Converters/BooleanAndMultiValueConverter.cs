﻿using System;
using System.Linq;
using Mpsi.Mobile.Core.Utility.MultiBinding;

namespace Mpsi.Mobile.Core.Utility.Converters
{
    public class BooleanAndMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values.Length < 1) return false;

            return values.OfType<bool>().Aggregate(true, (current, value) => current && value);
        }
    }
}
