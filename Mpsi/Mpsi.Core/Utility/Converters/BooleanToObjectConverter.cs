﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Mpsi.Mobile.Core.Utility.Converters
{
    // How to use:
    // <converters:BooleanToObjectConverter x:Key="colorConverter" x:TypeArguments="Color"
    //      TrueValue="#16CA86" FalseValue="#FF4081" />

    /// <summary>
    /// Boolean to object converter.
    /// </summary>
    public class BooleanToObjectConverter<T> : IValueConverter
    {
        public T FalseObject { set; get; }

        public T TrueObject { set; get; }

        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return (bool)value ? TrueObject : FalseObject;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return ((T)value).Equals(TrueObject);
        }
    }
}
