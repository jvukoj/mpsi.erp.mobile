﻿using System;

namespace Mpsi.Mobile.Core.Utility.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsNumber(this object value)
        {
            return value is sbyte
                   || value is byte
                   || value is short
                   || value is ushort
                   || value is int
                   || value is uint
                   || value is long
                   || value is ulong
                   || value is float
                   || value is double
                   || value is decimal;
        }

        public static bool IsDefaultValueOfNumber(this object value)
        {
            return
                value is sbyte && ((sbyte) value == 0)
                || value is byte && ((byte) value == 0)
                || value is short && ((short) value == 0)
                || value is ushort && ((ushort) value == 0)
                || value is int && ((int) value == 0)
                || value is uint && ((uint) value == 0)
                || value is long && ((long) value == 0L)
                || value is ulong && ((ulong) value == 0)
                || value is float && (Math.Abs((float) value) < Math.Pow(10, -6))
                || value is double && (Math.Abs((double) value) < Math.Pow(10, -6))
                || value is decimal && ((decimal) value == 0.0M);

        }
    }
}
