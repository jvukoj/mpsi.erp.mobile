﻿using System;
using System.Globalization;

namespace Mpsi.Mobile.Core.Utility.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Returns a value indicating whether the specified System.String object occurs
        /// within this string.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value">The string to seek.</param>
        /// <returns> 
        /// true if the value parameter occurs within this string, or if value is the empty
        /// string (""); otherwise, false.
        /// Also, false if source or value are null.
        /// </returns>
        public static bool CaseInsensitiveContains(this string source, string value)
        {
            if (source == null || value == null) return false;

            return CultureInfo.CurrentUICulture.CompareInfo.IndexOf(source, value, CompareOptions.IgnoreCase) >= 0;
        }

        public static string GetUntilOrEmpty(this string text, string stopAt)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return string.Empty;
        }
    }
}
