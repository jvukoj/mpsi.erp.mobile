﻿using System;
using System.Globalization;

namespace Mpsi.Mobile.Core.Utility.MultiBinding
{
    public interface IMultiValueConverter
    {
        object Convert(object[] values, Type targetType, object parameter, CultureInfo culture);
    }
}
