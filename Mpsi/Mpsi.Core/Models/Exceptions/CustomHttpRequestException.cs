﻿using System;
using System.Net;

namespace Mpsi.Mobile.Core.Models.Exceptions
{
    public class CustomHttpRequestException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public string Status { get; set; }

        public CustomHttpRequestException(string message, HttpStatusCode statusCode, string status) : base(message)
        {
            StatusCode = statusCode;
            Status = status;
        }
    }
}
