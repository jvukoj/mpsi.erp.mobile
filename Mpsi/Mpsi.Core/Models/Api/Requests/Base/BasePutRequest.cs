namespace Mpsi.Mobile.Core.Models.Api.Requests.Base
{
    public abstract class BasePutRequest : BaseRequest
    {
        public string Data { get; set; }
    }
}