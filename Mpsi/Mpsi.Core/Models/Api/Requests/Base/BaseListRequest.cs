namespace Mpsi.Mobile.Core.Models.Api.Requests.Base
{
    public abstract class BaseListRequest : BaseRequest
    {
        public string Column { get; set; }

        public string Region { get; set; }

        public string SearchTerm { get; set; } = "";

        public int Page { get; set; }

        public int Size { get; set; } = 25;

        public string Sort { get; set; } = "DESC"; // ASC or DESC
       
    }
}