namespace Mpsi.Mobile.Core.Models.Api.Requests.Base
{
    public abstract class BaseRequest
    {
        public bool InvalidateCache { get; set; }

        public bool UseCache { get; set; }

        public virtual string GetQueryString() => "";

        public abstract string GetRelativeUriPath();

        public string GetUri() => $"{GetRelativeUriPath()}{GetQueryString()}";

    }
}