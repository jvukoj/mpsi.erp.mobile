namespace Mpsi.Mobile.Core.Models.Api.Requests.Base
{
    public abstract class BaseDetailsRequest : BaseRequest
    {
        public long Id { get; set; } = 0;
    }
}