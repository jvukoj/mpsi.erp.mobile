﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Mpsi.Mobile.Core.Models.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AgreementStatus
    {
        [EnumMember(Value = "DRAFT")]
        Draft = 0,

        [EnumMember(Value = "SENT_FOR_APPROVAL")]
        InApproval,

        [EnumMember(Value = "APPROVED")]
        Approved,

        [EnumMember(Value = "REJECTED")]
        Rejected,

        [EnumMember(Value = "EXPORTED")]
        Exported,
    }
}
