﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Mpsi.Mobile.Core.Models.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CustomerType
    {
        None = 0,

        [EnumMember(Value = "RETAILER")]
        Retailer,

        [EnumMember(Value = "OEM")]
        Oem,
    }
}
