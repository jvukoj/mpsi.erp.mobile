﻿using Mpsi.Mobile.Core.Models.Enums;

namespace Mpsi.Mobile.Core.Models.NavigationParameters
{
    public class AuthenticationNavigationParameters : BaseNavigationParametersModel
    {
        public AuthenticationNavigationParameterActionTypes Action { get; }

        public string UserCertificateFilePath { get; }

        public string UserCertifcatePassword { get; }

        public AuthenticationNavigationParameters(AuthenticationNavigationParameterActionTypes action)
        {
            Action = action;
        }

        public AuthenticationNavigationParameters(AuthenticationNavigationParameterActionTypes action, string userCertificateFilePath, string userCertifcatePassword)
        {
            Action = action;
            UserCertificateFilePath = userCertificateFilePath;
            UserCertifcatePassword = userCertifcatePassword;
        }

        public AuthenticationNavigationParameters(Prism.Navigation.NavigationParameters navigationParameters)
        {
            AuthenticationNavigationParameterActionTypes action;
            string userCertificateFilePath;
            string userCertifcatePassword;
            navigationParameters.TryGetValue(nameof(Action), out action);
            navigationParameters.TryGetValue(nameof(UserCertificateFilePath), out userCertificateFilePath);
            navigationParameters.TryGetValue(nameof(UserCertifcatePassword), out userCertifcatePassword);

            Action = action;
            UserCertificateFilePath = userCertificateFilePath;
            UserCertifcatePassword = userCertifcatePassword;
        }

        public override Prism.Navigation.NavigationParameters ToNavigationParameters()
        {
            return new Prism.Navigation.NavigationParameters()
            {
                {nameof(Action), Action},
                {nameof(UserCertificateFilePath), UserCertificateFilePath},
                {nameof(UserCertifcatePassword), UserCertifcatePassword}
            };
        }
    }
}
