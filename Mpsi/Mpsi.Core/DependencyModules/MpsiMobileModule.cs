﻿using Autofac;
using Mpsi.Mobile.Core.Services;
using Mpsi.Mobile.Core.Services.Interfaces;

namespace Mpsi.Mobile.Core.DependencyModules
{
    internal class MpsiMobileModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DataCache>()
                .As<IDataCache>()
                .SingleInstance();

            builder.RegisterType<DataRegion>()
                .As<IDataRegion>()
                .SingleInstance();

            //builder.RegisterType<CacheProvider>()
            //    .As<ICacheProvider>()
            //    .SingleInstance();



            builder.RegisterType<Resiliency>()
                .As<IResiliency>()
                .InstancePerDependency();
        }
    }
}
