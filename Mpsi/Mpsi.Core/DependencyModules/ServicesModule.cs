﻿using Autofac;
using Mpsi.Mobile.Core.Services;
using Mpsi.Mobile.Core.Services.Interfaces;

namespace Mpsi.Mobile.Core.DependencyModules
{
    internal class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DialogService>()
                .As<IDialogService>()
                .InstancePerDependency();

            builder.RegisterType<ViewModelAggregateService>()
                .As<IViewModelAggregateService>()
                .InstancePerDependency();

            builder.RegisterType<AuthenticationService>()
                .As<IAuthenticationService>()
                .SingleInstance();
        }
    }
}
