﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Autofac;
using ModernHttpClient;
using Mpsi.Mobile.Core.Services;
using Mpsi.Mobile.Core.Services.Interfaces;
using Xamarin.Forms;

namespace Mpsi.Mobile.Core.DependencyModules
{
    public class HttpCommunicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<Func<HttpClient>>(
                    c => () =>
                    {
                        var cookieContainer = new CookieContainer();
                        var handler = GetHttpClientHandler(cookieContainer);

                        var httpClient = new HttpClient(handler)
                        {
                            BaseAddress = new Uri(GlobalSettings.Instance.BaseEndpoint)
                        };

                        httpClient.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/json"));

                        if (GlobalSettings.Instance.JSessionCookie != null)
                        {
                            cookieContainer.Add(httpClient.BaseAddress,
                                new Cookie(GlobalSettings.Instance.JSessionCookie.Name,
                                    GlobalSettings.Instance.JSessionCookie.Value));
                        }

                        return httpClient;
                    })
                .InstancePerLifetimeScope();

            // Register Request Provider
            builder.RegisterType<RequestProvider>()
                .As<IRequestProvider>()
                .InstancePerDependency();
        }

        private HttpClientHandler GetHttpClientHandler(CookieContainer cookieContainer)
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                return new NativeMessageHandler(throwOnCaptiveNetwork: false, customSSLVerification: true)
                {
                    CookieContainer = cookieContainer
                };
            }

            return new HttpClientHandler()
            {
                CookieContainer = cookieContainer
            };
        }
    }
}
