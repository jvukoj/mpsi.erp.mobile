﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mpsi.Mobile.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTabbedPage : TabbedPage
    {
        public MainTabbedPage()
        {
            InitializeComponent();
        }
    }
}