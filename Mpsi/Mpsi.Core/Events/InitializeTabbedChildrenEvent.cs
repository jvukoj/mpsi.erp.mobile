﻿using Prism.Events;
using Prism.Navigation;

namespace Mpsi.Mobile.Core.Events
{
    public class InitializeTabbedChildrenEvent : PubSubEvent<NavigationParameters>
    {

    }
}