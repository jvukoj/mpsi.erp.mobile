﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Mpsi.Mobile.Core.Resources;
using Mpsi.Mobile.Core.Models.Base;
using Mpsi.Mobile.Core.Services.Interfaces;
using Mpsi.Mobile.Core.ViewModels.Interfaces;
using Prism.Navigation;
using Xamarin.Forms;

namespace Mpsi.Mobile.Core.ViewModels.Base
{
    public abstract class ReadViewModel<TId, TItem> : ViewModelBase, IRefreshable
        where TItem : ModelBase
    {
        private TId _itemId;
        private TItem _item;

        protected ReadViewModel(IViewModelAggregateService aggregateService,
            INavigationService navigationService) : base(aggregateService, navigationService)
        {
        }

        public TId ItemId
        {
            get { return _itemId; }
            set { SetProperty(ref _itemId, value); }
        }

        public TItem Item
        {
            get { return _item; }
            set { SetProperty(ref _item, value); }
        }

        public ICommand RefreshCommand => new Command(async () => await RefreshAsync());

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            var id = OnNavigatedToOperationAction(parameters);

            await GetItemAsync(id);
        }

        /// <summary>
        /// Get info from navigation parameters and return ItemId.
        /// </summary>
        /// <param name="parameters"></param>
        public abstract TId OnNavigatedToOperationAction(NavigationParameters parameters);

        protected virtual async Task GetItemAsync(TId id)
        {
            if (EqualityComparer<TId>.Default.Equals(id, default(TId)))
            {
                await DialogService.ShowAlertAsync("ID is null.", AppResources.CommonError, AppResources.CommonClose);
                //await NavigationService.GoBackAsync();

                return;
            }

            if (IsBusy.Value) return;
            IsBusy.Value = true;

            ItemId = id;

            try
            {
                await GetItemOperationActionAsync();
            }
            catch (Exception ex)
            {
                await DialogService.ShowExceptionAlertAsync(ex.Message);
            }
            finally
            {
                IsBusy.Value = false;
            }
        }

        protected abstract Task GetItemOperationActionAsync();

        public virtual async Task RefreshAsync()
        {
            await GetItemAsync(ItemId);
        }
    }
}
