﻿using Prism.Mvvm;
using Prism.Navigation;
using Mpsi.Mobile.Core.Models.Base;
using Mpsi.Mobile.Core.Services.Interfaces;

namespace Mpsi.Mobile.Core.ViewModels.Base
{
    public abstract class ViewModelBase : BindableBase, INavigationAware
    {
        private Wrapper<bool> _isBusy = new Wrapper<bool>();


        protected readonly INavigationService NavigationService;

        protected readonly IDialogService DialogService;

        protected readonly IResiliency Resiliency;

        public ViewModelBase(IViewModelAggregateService aggregateService, INavigationService navigationService)
        {

            DialogService = aggregateService.DialogService;
            Resiliency = aggregateService.Resiliency;
            NavigationService = navigationService;
        }

        public Wrapper<bool> IsBusy
        {
            get { return _isBusy; }
            set { _isBusy = value; RaisePropertyChanged(); }
        }

        #region INavigationAware

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
            // Nothing to do.
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
            // Nothing to do.
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
            // Nothing to do.
        }

        #endregion
    }
}
