﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Navigation;
using Mpsi.Mobile.Core.Models.Base;
using Mpsi.Mobile.Core.Services.Interfaces;
using Mpsi.Mobile.Core.ViewModels.Interfaces;
using Xamarin.Forms;

namespace Mpsi.Mobile.Core.ViewModels.Base
{
    public abstract class ListViewModel<T> : ViewModelBase, IRefreshable, ISearchable
        where T : ModelBase
    {
        private ObservableCollection<T> _items;
        private T _selectedItem;
        private string _searchText;
        private bool _isRefreshing;
        private bool _canLoadMore;

        protected int PageNumber { get; set; }

        protected int TotalPageNumber { get; set; } = 1;

        protected ListViewModel(IViewModelAggregateService aggregateService,
            INavigationService navigationService) : base(aggregateService, navigationService)
        {
        }

        public ObservableCollection<T> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                RaisePropertyChanged();
            }
        }

        public T SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged();
            }
        }

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                RaisePropertyChanged();
            }
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                RaisePropertyChanged();
            }
        }

        public bool CanLoadMore
        {
            get { return _canLoadMore; }
            set
            {
                _canLoadMore = value;
                RaisePropertyChanged();
            }
        }

        public ICommand SearchCommand => new Command(Search);

        // Workaround for cleared text
        public ICommand SearchTextChangedCommand => new Command(SearchTextChanged);

        public ICommand RefreshCommand => new Command(async () => await RefreshAsync());

        public ICommand LoadMoreCommand => new Command(async () => await LoadMoreAsync());

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            OnNavigatedToOperationAction(parameters);

            CanLoadMore = true;

            await GetItemsAsync();
        }

        /// <summary>
        /// Get info from navigation parameters.
        /// </summary>
        /// <param name="parameters"></param>
        protected abstract void OnNavigatedToOperationAction(NavigationParameters parameters);

        protected async Task GetItemsAsync(int pageNumber = 0)
        {
            if (IsBusy.Value) return;
            IsBusy.Value = true;

            PageNumber = pageNumber;

            try
            {
                await GetItemsOperationActionAsync();
            }
            catch (Exception ex)
            {
                await DialogService.ShowExceptionAlertAsync(ex.Message);
            }
            finally
            {
                IsBusy.Value = false;
            }
        }

        protected abstract Task GetItemsOperationActionAsync();

        public virtual async Task RefreshAsync()
        {
            IsRefreshing = true;
            CanLoadMore = true;

            await GetItemsAsync();

            IsRefreshing = false;
        }

        private async Task LoadMoreAsync()
        {
            await GetItemsAsync(PageNumber + 1);

            // PageNumber is starting to count from 0. TotalPageNumber is starting to count from 1.
            // Like in list -> ordinal number (PageNumber) and count (TotalPageNumber)....
            CanLoadMore = PageNumber + 1 < TotalPageNumber;
        }

        public abstract void Search();

        protected abstract void SearchTextChanged();
    }
}
