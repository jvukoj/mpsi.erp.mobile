﻿using Mpsi.Mobile.Core.Events;
using Mpsi.Mobile.Core.Services.Interfaces;
using Mpsi.Mobile.Core.ViewModels.Base;
using Prism.Events;
using Prism.Navigation;

namespace Mpsi.Mobile.Core.ViewModels
{
    public class MainTabbedViewModel : ViewModelBase
    {
        private readonly IEventAggregator _eventAggregator;

        public MainTabbedViewModel(IViewModelAggregateService aggregateService, IEventAggregator eventAggregator,
            INavigationService navigationService) : base(aggregateService, navigationService)
        {
            _eventAggregator = eventAggregator;
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            _eventAggregator.GetEvent<InitializeTabbedChildrenEvent>().Publish(parameters);
        }
    }
}
