﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Navigation;
using Siemens.PL.Models.Api.Requests;
using Siemens.PL.Models.Api.Responses;
using Siemens.PL.Models.NavigationParameters;
using Siemens.PL.Services.Interfaces;
using Siemens.PL.ViewModels.Base;
using Siemens.PL.Views;
using Siemens.PL.Utility.Extensions;
using Xamarin.Forms;

namespace Siemens.PL.ViewModels
{
    public class AgreementListPageViewModel : ListViewModel<Agreement>
    {
        private List<Agreement> _allAgreements;
        private readonly IAgreementService _agreementService;

        public AgreementListPageViewModel(IViewModelAggregateService aggregateService,
            INavigationService navigationService, IAgreementService agreementService) : base(aggregateService,
            navigationService)
        {
            _agreementService = agreementService;
        }

        public ICommand ShowAgreementDetailsCommand => new Command(async () =>
        {
            if (SelectedItem == null) return;

            await NavigationService.NavigateAsync(nameof(AgreementReadPage),
                new AgreementReadNavigationParameters(agreementId: SelectedItem.Id).ToNavigationParameters());
        });

        protected override void OnNavigatedToOperationAction(NavigationParameters parameters)
        {
            // Nothing to do.
        }

        protected override async Task GetItemsOperationActionAsync()
        {
            await Resiliency.TryAsync(async () =>
            {
                SearchText = string.Empty;

                // Set request data
                var request = new AgreementListGetRequest
                {
                    Region = GlobalSettings.Instance.ClientRegion,
                    Owner = GlobalSettings.Instance.ClientGid,
                    Page = PageNumber,
                };

                var response = await _agreementService.GetAgreementsAsync(request);
                if (response?.Content == null)
                {
                    Items = null;
                    return;
                }

                TotalPageNumber = response.TotalPages;

                _allAgreements = PageNumber == 0
                    ? new List<Agreement>()
                    : (_allAgreements ?? new List<Agreement>());

                _allAgreements.AddRange(response.Content.ToList());
                Items = _allAgreements.ToObservableCollection();
            });
        }

        public override void Search()
        {
            if (string.IsNullOrWhiteSpace(SearchText))
            {
                Items = _allAgreements?.ToObservableCollection();
                return;
            }

            var items = _allAgreements?.Where(x =>
                x.AgreementId.CaseInsensitiveContains(SearchText)
                || x.Status.ToString().CaseInsensitiveContains(SearchText)
                || x.SapCustomerId.CaseInsensitiveContains(SearchText)
                || (x.AgreementCustomerData?.CustomerName).CaseInsensitiveContains(SearchText)
                || (x.AgreementData?.ProjectName).CaseInsensitiveContains(SearchText));

            Items = items?.ToObservableCollection();
        }

        protected override void SearchTextChanged()
        {
            if (!string.IsNullOrWhiteSpace(SearchText)) return;

            Items = _allAgreements?.ToObservableCollection();
        }
    }
}
