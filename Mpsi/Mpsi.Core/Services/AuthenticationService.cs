﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mpsi.Mobile.Core.Models.Api.Requests;
using Mpsi.Mobile.Core.Models.Api.Responses;
using Mpsi.Mobile.Core.Models.Enums;
using Mpsi.Mobile.Core.Services.Interfaces;


namespace Mpsi.Mobile.Core.Services
{
    public class AuthenticationService : IAuthenticationService
    {


        private readonly IRequestProvider _requestProvider;
        private readonly IDataCache _dataCache;
        private readonly IDialogService _dialogService;

        public AuthenticationService(IRequestProvider requestProvider, IDataCache dataCache, IDialogService dialogService)
        {
            _requestProvider = requestProvider;
            _dataCache = dataCache;
            _dialogService = dialogService;
        }


        public async Task<LoginPostResponse> LoginUser(LoginPostRequest request)
        {
            var path = request.GetUri();
            List<KeyValuePair<string,string>> formParameters = new List<KeyValuePair<string, string>>();
            formParameters.Add( new KeyValuePair<string, string>(request.GrantTypeKey, request.GrantTypeValue));
            formParameters.Add(new KeyValuePair<string, string>(request.UserNameKey, request.UserNameValue));
            formParameters.Add(new KeyValuePair<string, string>(request.PasswordKey, request.PasswordValue));
            formParameters.Add(new KeyValuePair<string, string>(request.ClientIdKey, request.ClientIdValue));
            formParameters.Add(new KeyValuePair<string, string>(request.DeviceSerialNumberKey, request.DeviceSerialNumberValue));

            List<KeyValuePair<string, string>> headerParameters = new List<KeyValuePair<string, string>>();
            headerParameters.Add(new KeyValuePair<string, string>(request.ContentTypeKey, request.ContentTypeValue));

            return await _requestProvider.PostAsync<LoginPostResponse>(path, formParameters, headerParameters);
        }

    }
}
