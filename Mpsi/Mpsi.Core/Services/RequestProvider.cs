﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Mpsi.Mobile.Core.Models;
using Mpsi.Mobile.Core.Models.Exceptions;
using Mpsi.Mobile.Core.Services.Interfaces;


namespace Mpsi.Mobile.Core.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class RequestProvider : IRequestProvider
    {
        private readonly Func<HttpClient> _httpClientFunc;
        private readonly JsonSerializerSettings _serializerSettings;

        public RequestProvider(Func<HttpClient> httpClientFunc)
        {
            _httpClientFunc = httpClientFunc;

            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore,
                Converters = new List<JsonConverter> { new StringEnumConverter { CamelCaseText = true } },

            };
            _serializerSettings.Converters.Add(new StringEnumConverter());
        }

        public async Task<TResult> GetAsync<TResult>(string path)
        {
            using (var httpClient = _httpClientFunc())
            {

                var response = await httpClient.GetAsync(path);

                await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                return result;
            }
        }

        public async Task<TResult> PostAsync<TResult>(string uri, List<KeyValuePair<string, string>> formParameters, List<KeyValuePair<string, string>> headerParameters)
        {
            using (var httpClient = _httpClientFunc())
            {
                FormUrlEncodedContent content = new FormUrlEncodedContent(formParameters);
                content.Headers.ContentType = new MediaTypeHeaderValue(headerParameters.FirstOrDefault(x => x.Key == "Content-Type").Value);
                
                foreach (KeyValuePair<string,string> kvp in headerParameters)
                {
                    if(kvp.Key != "Content-Type")
                        content.Headers.Add(kvp.Key, kvp.Value);
                }

                var response = await httpClient.PostAsync(uri, content);

                await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                return result;
            }
        }

        public async Task<TResult> PutAsync<TResult>(string uri, string data)
        {
            using (var httpClient = _httpClientFunc())
            {
                //if (!string.IsNullOrEmpty(header))
                //{
                //    AddHeaderParameter(httpClient, header);
                //}

                //var content = new StringContent(JsonConvert.SerializeObject(data));
                var content = new StringContent(data);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = await httpClient.PutAsync(uri, content);

                await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                return result;
            }
        }

        public async Task<TResult> PostAsync<TResult>(string uri, string data)
        {
            using (var httpClient = _httpClientFunc())
            {
                var content = new StringContent(data);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/text");

                var response = await httpClient.PostAsync(uri, content);

                await HandleResponse(response);
                var serialized = await response.Content.ReadAsStringAsync();

                var result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                return result;
            }
        }

        private void AddHeaderParameter(HttpClient httpClient, string parameter)
        {
            if (httpClient == null) return;

            if (string.IsNullOrEmpty(parameter)) return;

            httpClient.DefaultRequestHeaders.Add(parameter, Guid.NewGuid().ToString());
        }

        private async Task HandleResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode) return;

            try
            {
                var content = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.Forbidden ||
                    response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new ServiceAuthenticationException(content);
                }

                // TODO: Lea: Hope this is temporary :/
                if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new HttpRequestException(content);
                }

                // Note: Not sure if every content will have same structure. Possible JsonSerializationException!
                var result = JsonConvert.DeserializeObject<HttpContentModel>(content, _serializerSettings);

                throw new CustomHttpRequestException(result.ErrorDescription, response.StatusCode, result.Error);
            }
            catch (JsonReaderException)
            {
                throw new CustomHttpRequestException(response.ReasonPhrase, response.StatusCode, response.StatusCode.ToString());
            }
        }
    }
}
