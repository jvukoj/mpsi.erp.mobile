﻿
using System.Threading.Tasks;
using Mpsi.Mobile.Core.Models.Api.Requests;
using Mpsi.Mobile.Core.Models.Api.Responses;
using Mpsi.Mobile.Core.Models.Enums;

namespace Mpsi.Mobile.Core.Services.Interfaces
{
    public interface IAuthenticationService
    {
        Task<LoginPostResponse> LoginUser(LoginPostRequest request);

    }
}
