﻿using System;
using System.Collections.Generic;

namespace Mpsi.Mobile.Core.Services.Interfaces
{
    public interface IDataCache
    {
        /// <summary>
        /// Dictionary holding all the regions of the cache.
        /// </summary>
        IDictionary<string, DataRegion> Regions { get; set; }

        /// <summary>
        /// The default time that an object is kept in the cache before it expires.
        /// </summary>
        TimeSpan DefaultExpiryPeriod { get; set; }

        /// <summary>
        /// Adds an object to the default region in the cache.
        /// </summary>
        /// <param name="key">A unique value that is used to store and retrieve the object from the cache.</param>
        /// <param name="value">The object saved to the cache.</param>
        void Add(string key, object value);

        /// <summary>
        /// Adds an object to a region in the cache. If the region doesn't exist it's created.
        /// </summary>
        /// <param name="key">A unique value that is used to store and retrieve the object from the cache. </param>
        /// <param name="value">The object saved to the cache.</param>
        /// <param name="region">The name of the region to save the object in.</param>
        void Add(string key, object value, string region);

        /// <summary>
        /// Removes an object to the default region in the cache.
        /// </summary>
        /// <param name="key">A unique value that is used to store and retrieve the object from the cache.</param>
        void Remove(string key);

        /// <summary>
        /// Removes an object to a region in the cache. If the region doesn't exist it's created.
        /// </summary>
        /// <param name="key">A unique value that is used to store and retrieve the object from the cache. </param>
        /// <param name="region">The name of the region to remove the object from.</param>
        void Remove(string key, string region);

        /// <summary>
        /// Deletes all objects in the specified region.
        /// </summary>
        /// <param name="region">The name of the region whose objects are removed.</param>
        void ClearRegion(string region);

        /// <summary>
        /// Creates a region.
        /// </summary>
        /// <param name="region">The name of the region that is created.</param>
        /// <returns>If the region has been created successfully or not. Should the region already exist, this method will return false.</returns>
        bool CreateRegion(string region);

        /// <summary>
        /// Checks if the region exists.
        /// </summary>
        /// <param name="region">The name of the region that needs to be checked.</param>
        /// <returns>If the region exists or not.</returns>
        bool ContainsRegion(string region);

        /// <summary>
        /// Gets an object from the cache using the specified key from the default region.
        /// </summary>
        /// <param name="key">The unique value that is used to identify the object in the cache.</param>
        /// <returns>The object that was cached by using the specified key. Null is returned if the key does not exist.</returns>
        object Get(string key);

        /// <summary>
        /// Gets an object from the specified region by using the specified key. 
        /// </summary>
        /// <param name="key">The unique value that is used to identify the object in the region.</param>
        /// <param name="region">The name of the region where the object resides.</param>
        /// <returns>The object that was cached by using the specified key. Null is returned if the key does not exist.</returns>
        object Get(string key, string region);

        /// <summary>
        /// Gets an object from the cache using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the object to be retrieved.</typeparam>
        /// <param name="key">The unique value that is used to identify the object in the region.</param>
        /// <returns>The object that was cached by using the specified key. Null is returned if the key does not exist.</returns>
        T Get<T>(string key);

        /// <summary>
        /// Gets an object from the cache using the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the object to be retrieved.</typeparam>
        /// <param name="key">The unique value that is used to identify the object in the region.</param>
        /// <param name="region">The name of the region where the object resides.</param>
        /// <returns>The object that was cached by using the specified key. Null is returned if the key does not exist.</returns>
        T Get<T>(string key, string region);

        /// <summary>
        /// Gets an enumerable list of all cached objects in the specified region.
        /// </summary>
        /// <param name="region">The name of the region for which to return a list of all resident objects.</param>
        /// <returns>An enumerable list of all cached objects in the specified region.</returns>
        IEnumerable<KeyValuePair<string, object>> GetObjectsInRegion(string region);
    }
}