using System;
using System.Threading.Tasks;
using Mpsi.Mobile.Core.Models.Base;

namespace Mpsi.Mobile.Core.Services.Interfaces
{
    public interface IResiliency
    {
        void Try(Action action);

        void Try(Action action, Action finallyAction);

        Task TryAsync(Func<Task> action);

        Task TryAsync(Wrapper<bool> isBusy, Func<Task> action);

        Task TryAsync(Func<Task> action, Action finallyAction);

        Task TryAsync(Wrapper<bool> isBusy, Func<Task> action, Action finallyAction);
    }
}