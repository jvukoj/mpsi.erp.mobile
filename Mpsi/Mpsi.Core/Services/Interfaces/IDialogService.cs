﻿using System.Threading.Tasks;
using Acr.UserDialogs;

namespace Mpsi.Mobile.Core.Services.Interfaces
{
    public interface IDialogService
    {
        Task ShowAlertAsync(string message, string title, string buttonText);

        Task ShowExceptionAlertAsync(string message);

        Task<PromptResult> ShowPromptAsync(string message, string title, string okText, string cancelText, string placeholder);

        Task<bool> ShowConfirmAsync(string message, string title, string buttonText);
    }
}
