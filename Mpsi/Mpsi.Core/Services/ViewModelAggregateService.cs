using Mpsi.Mobile.Core.Services.Interfaces;

namespace Mpsi.Mobile.Core.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ViewModelAggregateService : IViewModelAggregateService
    {


        public IDialogService DialogService { get; }

        public IResiliency Resiliency { get; }

        public ViewModelAggregateService(IDialogService dialogService, IResiliency resiliency)
        {

            DialogService = dialogService;
            Resiliency = resiliency;
        }
    }
}