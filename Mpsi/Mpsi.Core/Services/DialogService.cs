﻿using System.Threading.Tasks;
using Acr.UserDialogs;
using Mpsi.Mobile.Core.Resources;
using Mpsi.Mobile.Core.Services.Interfaces;

namespace Mpsi.Mobile.Core.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class DialogService : IDialogService
    {
        public Task ShowAlertAsync(string message, string title, string buttonText)
        {
            return UserDialogs.Instance.AlertAsync(message, title, buttonText);
        }

        public Task ShowExceptionAlertAsync(string message)
        {
            return UserDialogs.Instance.AlertAsync(message, AppResources.CommonError, AppResources.CommonClose);
        }

        public Task<PromptResult> ShowPromptAsync(string message, string title, string okText, string cancelText, string placeholder)
        {
            return UserDialogs.Instance.PromptAsync(message, title, okText, cancelText, placeholder);
        }

        public Task<bool> ShowConfirmAsync(string message, string title, string buttonText)
        {
            return UserDialogs.Instance.ConfirmAsync(message, title, buttonText);
        }
    }
}
