﻿using System;
using Siemens.PL.Services.Interfaces;
using Mpsi.Mobile.iOS.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(LoggerService))]

namespace Mpsi.Mobile.iOS.Services
{
    public class LoggerService : ILogger
    {
        private const string MethodStartTemplate = "{0}.{1}() - START";
        private const string MethodTemplate = "{0}.{1}()";
        private const string MethodEndTemplate = "{0}.{1}() - END";

        public void TraceDebug(string message) => Console.WriteLine(message);

        public void TraceInfo(string message) => Console.WriteLine(message);

        public void TraceWarning(string message) => Console.WriteLine(message);

        public void TraceError(string message, Exception ex = null) =>
            Console.Error.WriteLine($"{message}{Environment.NewLine}{ex}");

        public void TraceFatal(string message, Exception ex = null) =>
            Console.Error.WriteLine($"{message}{Environment.NewLine}{ex}");

        public void TraceMethod(string className, string methodName = "") =>
            Console.WriteLine(MethodTemplate, className, methodName);

        public void TraceMethodWithMessage(string message, string className, string methodName = "") =>
            Console.WriteLine(@"{0}. {1}", string.Format(MethodTemplate, className, methodName), message);

        public void TraceMethodStart(string className, string methodName = "") =>
            Console.WriteLine(MethodStartTemplate, className, methodName);

        public void TraceMethodStartWithMessage(string message, string className, string methodName = "") =>
            Console.WriteLine(@"{0}. {1}", string.Format(MethodStartTemplate, className, methodName), message);

        public void TraceMethodEnd(string className, string methodName = "") =>
            Console.WriteLine(MethodEndTemplate, className, methodName);

        public void TraceMethodEndWithMessage(string message, string className, string methodName = "") =>
            Console.WriteLine(@"{0}. {1}", string.Format(MethodEndTemplate, className, methodName), message);
    }
}
