﻿using System;

namespace Mpsi.Mobile.iOS.Models.CustomEventArgs
{
    public class FailedWithErrorEventArgs : EventArgs
    {
        public string ErrorMessage { get; }

        public FailedWithErrorEventArgs(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}