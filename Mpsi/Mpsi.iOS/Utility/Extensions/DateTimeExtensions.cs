﻿using System;
using Foundation;

namespace Mpsi.Mobile.iOS.Utility.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime NSDateToDateTime(NSDate date, DateTime defaultValue)
        {
            if (date == null) return defaultValue;

            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
                new DateTime(2001, 1, 1, 0, 0, 0));
            return reference.AddSeconds(date.SecondsSinceReferenceDate);
        }
    }
}
