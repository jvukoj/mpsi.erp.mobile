﻿using System.Collections.Generic;
using System.Net;
using Autofac.Core;
using Foundation;
using Mpsi.Mobile.Core;
using Mpsi.Mobile.iOS.DependencyModules;
using UIKit;

namespace Mpsi.Mobile.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Style();

            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App(new IModule[] { new PlatformSpecificModule() }));

            return base.FinishedLaunching(app, options);
        }

        private void Style()
        {
            var highlightColor = UIColor.FromRGBA(0, 130, 130, 1);
            var accentBaseColor = UIColor.FromRGBA(0, 147, 147, 1);
            var accentColor = UIColor.FromRGBA(0, 147, 147, 204);
            var accentColor2 = UIColor.FromRGBA(0, 147, 147, 153);
            var accentColor3 = UIColor.FromRGBA(0, 147, 147, 102);
            var accentColor4 = UIColor.FromRGBA(0, 147, 147, 51);


            UINavigationBar.Appearance.BarTintColor = accentBaseColor;
            UINavigationBar.Appearance.TintColor = UIColor.White;
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes() { TextColor = UIColor.White });

            //UISwitch.Appearance.OnTintColor = highlightColor;
            //UISwitch.Appearance.ThumbTintColor = accentColor4;

            //UITabBar.Appearance.BarTintColor = UIColor.Red;

            //UIButton.Appearance.TintColor = highlightColor;
            //UIButton.Appearance.SetTitleColor(highlightColor, UIControlState.Normal);
        }

        public override void OnResignActivation(UIApplication uiApplication)
        {
            base.OnResignActivation(uiApplication);

            // Prevent taking snapshot
            uiApplication.IgnoreSnapshotOnNextApplicationLaunch();

            // Add blur view when app is going in background
            UIVisualEffect blurEffect = UIBlurEffect.FromStyle(UIBlurEffectStyle.ExtraDark);
            UIVisualEffectView visualEffectView = new UIVisualEffectView(blurEffect) { Tag = 1938 };
            visualEffectView.Frame = uiApplication.KeyWindow.Bounds;
            uiApplication.KeyWindow.AddSubview(visualEffectView);
        }

        // Bring back main interface from obscuring
        public override void OnActivated(UIApplication uiApplication)
        {
            base.OnActivated(uiApplication);

            // Find our hiding view by the same tag we saw earlier
            var view = uiApplication.KeyWindow.ViewWithTag(1938);

            // If we found it, hide it again!
            if (view != null)
            {
                // Animate it back to transparent
                UIView.Animate(0.5, () =>
                {
                    view.Alpha = 0;
                }, () =>
                {
                    // And after that completed, remove te view altogether
                    view.RemoveFromSuperview();
                });
            }
        }
    }
}
